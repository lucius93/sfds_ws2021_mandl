"""
====================================================================
Author:         Lucas Mandl
Company:        FH Joanneum
Date:           03.10.2020
Description:    Implementation of laboratory session 3 - P.2 Read multiple CSVs
====================================================================
"""
# =================== Imports ==============================
import os
import pandas as pd

# =================== Preamble =============================
# clear command line and workspace:                        # 
try:                                                       #
    from IPython import get_ipython                        #
    get_ipython().magic('clear')                           #
    get_ipython().magic('reset -f')                        #
except:                                                    #
    pass                                                   #
                                                           #
# set working directory to current file location           #
abspath = os.path.abspath(__file__)                        #
dname = os.path.dirname(abspath)                           #
os.chdir(dname)                                            #
# ==========================================================

os.chdir( "Beispiel5")
# get filenames in directory
files = os.listdir()

df_files = []
for f in files:
    df_files.append(pd.read_csv(f))