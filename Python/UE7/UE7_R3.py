"""
====================================================================
Author:         Lucas Mandl
Company:        FH Joanneum
Date:           02.11.2020
Description:    Implementation of laboratory session 7 - R.3 Datamanipulation in R
====================================================================
"""
# =================== Imports ==============================
import os
import pandas as pd

# =================== Preamble =============================
# clear command line and workspace:
try:
    from IPython import get_ipython
    get_ipython().magic('clear')
    get_ipython().magic('reset -f')
except:
    pass

# set working directory to curent file location
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)
# ==========================================================

presidents_html = pd.read_html("https://en.wikipedia.org/wiki/List_of_presidents_of_the_United_States")
presidents = presidents_html[1][["Election","President.1"]]

presidents.drop(presidents.tail(1).index,inplace=True) # drop last line

presidents.to_csv(dname+"\\"+"presidents.csv", index=False)