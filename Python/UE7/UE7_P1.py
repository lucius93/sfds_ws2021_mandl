"""
====================================================================
Author:         Lucas Mandl
Company:        FH Joanneum
Date:           03.10.2020
Description:    Implementation of laboratory session 7 - P.1 CSV in Python
====================================================================
"""
# =================== Imports ==============================
import os
import pandas as pd
import numpy as np

# =================== Preamble =============================
# clear command line and workspace:                        # 
try:                                                       #
    from IPython import get_ipython                        #
    get_ipython().magic('clear')                           #
    get_ipython().magic('reset -f')                        #
except:                                                    #
    pass                                                   #
                                                           #
# set working directory to curent file location            #
abspath = os.path.abspath(__file__)                        #
dname = os.path.dirname(abspath)                           #
os.chdir(dname)                                            #
# ==========================================================

# get filenames in directory
files = os.listdir(dname)

P1 = pd.read_csv(dname+"\\"+files[2], encoding= 'unicode_escape')

manufacturer = np.array(P1.manufacturer)
model = np.array(P1.model)
displ = np.array(P1.displ)
