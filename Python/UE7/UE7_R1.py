"""
====================================================================
Author:         Lucas Mandl
Company:        FH Joanneum
Date:           02.11.2020
Description:    Implementation of laboratory session 3 - R.1 CSVs in R and Python
====================================================================
"""
# =================== Imports ==============================
import os
import pandas as pd

# =================== Preamble =============================
# clear command line and workspace:
try:
    from IPython import get_ipython
    get_ipython().magic('clear')
    get_ipython().magic('reset -f')
except:
    pass

# set working directory to curent file location
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)
# ==========================================================

# get filenames in directory
files = os.listdir(dname)

houses = pd.read_csv(dname+"\\"+files[0], encoding= 'unicode_escape', skiprows = 1306)
houses2 = pd.read_table(dname+"\\"+files[0], encoding= 'unicode_escape', skiprows = 1306)