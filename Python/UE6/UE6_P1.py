"""
====================================================================
Author:         Lucas Mandl
Company:        FH Joanneum
Date:           07.11.2020
Description:    Implementation of laboratory session 6 - R.1 Classes: classical mathematical operations
====================================================================
"""
# =================== Imports ==============================
import os

# =================== Preamble =============================
# clear command line and workspace:
try:
    from IPython import get_ipython
    get_ipython().magic('clear')
    get_ipython().magic('reset -f')
except:
    pass

# set working directory to curent file location
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)
# ==========================================================

# make class with basic mathematical functionality
class My_Calculations:
    def __init__(self,y):
        self.y = y
        
    def my_mul(self, x):
        return x*self.y

    def my_div(self, x):
        return x/self.y
    
    def my_add(self, x):
        return x + self.y
    
    def my_sub(self, x):
        return x - self.y
    
    
# test class My_Calulations:
def test_my_class():
    x = 2
    y = 3
    my_cal = My_Calculations(y)
    
    if my_cal.my_mul(x) != x*y:
        print("Error in my_mul")
        
    if my_cal.my_div(x) != x/y:
        print("Error in my_mul")
      
    if my_cal.my_add(x) != x + y:
        print("Error in my_mul")
        
    if my_cal.my_sub(x) != x - y:
        print("Error in my_mul")
        
test_my_class()