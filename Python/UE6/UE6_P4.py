"""
====================================================================
Author:         Lucas Mandl
Company:        FH Joanneum
Date:           11.11.2020
Description:    Implementation of laboratory session 6 - R.3 Classes: points in 2d
====================================================================
"""
# =================== Imports ==============================
import os
import math as m

# =================== Preamble =============================
# clear command line and workspace:
try:
    from IPython import get_ipython
    get_ipython().magic('clear')
    get_ipython().magic('reset -f')
except:
    pass

# set working directory to curent file location
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)
# ==========================================================

class Point:
    def __init__(self, *my_points):
        self.points = my_points

p1 = Point(1,1)