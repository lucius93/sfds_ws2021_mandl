"""
====================================================================
Author:         Lucas Mandl
Company:        FH Joanneum
Date:           07.11.2020
Description:    Implementation of laboratory session 6 - R.2 Classes: extension to fractures
====================================================================
"""
# =================== Imports ==============================
import os

# =================== Preamble =============================
# clear command line and workspace:
try:
    from IPython import get_ipython
    get_ipython().magic('clear')
    get_ipython().magic('reset -f')
except:
    pass

# set working directory to curent file location
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)
# ==========================================================

# get greatest common divisor:
def gcd(a, b):
    if a == 0:
        return b
    else:
        while b != 0:
            if a > b:
                a = a - b
            else:
                b = b - a
    
    return a

class My_Frac:
    def __init__(self, num, denum):
        if denum != 0:
            self.num = num
            self.denum = denum
        else:
            print("Error division by zero")
        
    def my_mul(self,num,denum):
        if denum != 0:
            new_num = self.num*num
            new_denum = self.denum*denum
            print(new_num,"/",new_denum)
        else:
            print("Error division by zero")
    
    def my_div(self,num,denum):
        if denum != 0:
            new_num = self.num*denum
            new_denum = self.denum*num
            print(new_num,"/",new_denum)
        else:
            print("Error division by zero")
        
    def my_sub(self,num,denum):
        if denum != 0:
            new_num = self.num*denum - self.denum*num
            new_denum = self.denum*denum
            print(new_num,"/",new_denum)
        else:
            print("Error division by zero")

    
    def my_reduc(self):
        print(self.num/gcd(self.num,self.denum),"/",self.denum/gcd(self.num,self.denum))
    
frac = My_Frac(1651, 3416)

frac.my_mul(3,2)
frac.my_div(3,2)
frac.my_sub(3,2)
frac.my_reduc()
