"""
====================================================================
Author:         Lucas Mandl
Company:        FH Joanneum
Date:           11.11.2020
Description:    Implementation of laboratory session 6 - R.3 Classes: points in 2d
====================================================================
"""
# =================== Imports ==============================
import os
import math as m

# =================== Preamble =============================
# clear command line and workspace:
try:
    from IPython import get_ipython
    get_ipython().magic('clear')
    get_ipython().magic('reset -f')
except:
    pass

# set working directory to curent file location
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)
# ==========================================================

class Point:
    def __init__(self, x , y):
        self.x = x
        self.y = y
        
    # overloading + and - operator:
    def __add__(self, o):
        return self.x + o.x, self.y + o.y
    
    def __sub__(self, o):
        return self.x - o.x, self.y - o.y
        
    def my_dist(self, x, y):
        return m.sqrt(pow(x-self.x, 2) + pow(y-self.y,2))
    
    def print_point(self):
        print("(",self.x,",",self.y,")")
        
    def add_point(self, x, y):
        self.x = self.x + x
        self.y = self.y + y
    
    def subs_point(self, x, y):
        self.x = self.x - x
        self.y = self.y - y
        
        
p1 = Point(1,1)
p2 = Point(2,2)

# operator overloading:
p3 = p1 + p2
p4 = p1 - p2

print(p1.my_dist(2,2))
