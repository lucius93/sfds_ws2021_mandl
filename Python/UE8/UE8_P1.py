"""
====================================================================
Author:         Lucas Mandl
Company:        FH Joanneum
Date:           03.10.2020
Description:    Implementation of laboratory session 8 - P.1 Pandas
====================================================================
"""
# =================== Imports ==============================
import os
import pandas as pd
import numpy as np

# =================== Preamble =============================
# clear command line and workspace:                        # 
try:                                                       #
    from IPython import get_ipython                        #
    get_ipython().magic('clear')                           #
    get_ipython().magic('reset -f')                        #
except:                                                    #
    pass                                                   #
                                                           #
# set working directory to curent file location            #
abspath = os.path.abspath(__file__)                        #
dname = os.path.dirname(abspath)                           #
os.chdir(dname)                                            #
# ==========================================================

# get filenames in directory
files = os.listdir(dname)

menu = pd.read_csv(dname+"\\"+files[0], encoding= 'unicode_escape')
toRead = ['Category', 'Item']
menu_filtered = menu[toRead]
# 'Category', 'Item', 'Serving Size', 'Calories', 'Calories from Fat', 'Total Fat', 'Saturated Fat (% Daily Value)', 'Trans Fat', ’Cholesterol’, ’Cholesterol (% Daily Value)’, ’Sodium’, ’Sodium (% Daily Value)’, ’Sugars’, ’Protein’, 'Vitamin C (% Daily Value)', 'Calcium (% Daily Value)'