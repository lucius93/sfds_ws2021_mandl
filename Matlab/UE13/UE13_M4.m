% Exercise 13 - M.4
% Mandl Lucas 
% 07.12.2020
clc 
clear all

n = 100;
disp("Sum1:")
tic
sum1(n)
toc
disp("Sum2:")
tic
sum2(n)
toc

n = 1000;
disp("Sum1:")
tic
sum1(n)
toc
disp("Sum2:")
tic
sum2(n)
toc

n = 10000;
disp("Sum1:")
tic
sum1(n)
toc
disp("Sum2:")
tic
sum2(n)
toc

function [val] = sum1(n)
    val = 0;
    for i = 1:n
        val = val + i;
    end
end

function [val] = sum2(n)
    val = n*(n+1)/2;
end