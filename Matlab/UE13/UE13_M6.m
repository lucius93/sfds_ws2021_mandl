% Exercise 13 - M.4
% Mandl Lucas 
% 07.12.2020
clc 
clear all

A1 = [1,2,3;4,5,6];
B1 = [1,2;4,5;7,8];
X1 = matmul(A1,B1)
A1*B1

A2 = [1,2,3;4,5,6;7,8,9]
B2 = [1,2;3,4;5,6]
X2 = matmul(A2,B2)
A2*B2

A3 = [2,3;5,6;8,9]
B3 = [1,2;3,4;5,6]
X3 = matmul(A3,B3)
try
    A3*B3
catch
    warning("Matrix dimension mismatch")
end

function [mat] = matmul(A,B)
    [na, ma] = size(A);
    [nb, mb] = size(B);

    if(ma == nb)%&& ma == nb)
        % implementation of: C_ij = Sum(A_ik*B_kj)
        % i iterates columns of A
        % j iterates rows of B
        % k iterates rows of A
        C = zeros(na,mb);
        for i = 1:na

            for j = 1:mb
                sum = 0;
                for k = 1:ma
                    sum = sum + A(i,k)*B(k,j);
                end
                C(i,j) = sum;
            end
        end
        mat = C;
    else
        mat = warning("Matrix dimension mismatch");
    end
end
