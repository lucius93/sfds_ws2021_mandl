% Exercise 13 - M.4
% Mandl Lucas 
% 07.12.2020
clc 
clear all

A = [1,2,3;4,5,6;7,8,9];
B = [1,2;4,5];

a = my_det(A)
det(A)

b = my_det(B)
det(B)

function [A] = calc_22_det(X)
    A = X(1,1)*X(2,2) - X(2,1)*X(1,2); 
end

function [A] = calc_33_det(X)
    a = X(1,1)*X(2,2)*X(3,3) + X(1,2)*X(2,3)*X(3,1) + X(1,3)*X(2,1)*X(3,2);
    b = -X(3,1)*X(2,2)*X(1,3) - X(3,2)*X(2,3)*X(1,1) - X(3,3)*X(2,1)*X(1,2);
    A = a + b;
end

function [B] = my_det(A)
    [num_rows, num_cols] = size(A);
    if(num_rows == 2 && num_cols == 2)
        B = calc_22_det(A);
    elseif(num_rows == 3 && num_cols == 3)
        B = calc_33_det(A);
    else
        disp("Error");
        B = NaN;
    end
end