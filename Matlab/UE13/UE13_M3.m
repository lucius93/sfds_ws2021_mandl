% Exercise 13 - M.3
% Mandl Lucas 
% 07.12.2020
clc 
clear all

sin_int(1) = integrate_f(100  , 0, 2*pi, "sin");
sin_int(2) = integrate_f(1000 , 0, 2*pi, "sin");
sin_int(3) = integrate_f(10000, 0, 2*pi, "sin");

x_int(1) = integrate_f(100  , 2, 5, "x");
x_int(2) = integrate_f(1000 , 2, 5, "x");
x_int(3) = integrate_f(10000, 2, 5, "x");

%% analytic solutions:
% sin^2(x):
a = 2*pi;
I_sin = 1/2*(a-2*sin(a)*cos(a))-(1/2*(0-2*sin(0)*cos(0)));

% x^2
a = 2;
b = 5;       
I_x = b*b*b/3-a*a*a/3; 

%% numeric solution with trapezoidal rule (trapezoids are used to approximate the area under the curve)
function [val] = integrate_f(N,a,b,f)
    h = (b-a)/N;
    I = 0;
    for i =1:N
        xi = a + (i-1)*h;  % x_i
        xi1 = a + i*h;     % x_(i+1)
        if (f == "sin")
            I = I + h/2*(sin(xi)*sin(xi) + sin(xi1)*sin(xi1));
        elseif(f == "x")
            I = I + h/2*(xi*xi + (xi1)*(xi1));
        end
    end
    val = I;
end
    
    