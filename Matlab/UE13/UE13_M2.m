% Exercise 13 - M.2
% Mandl Lucas 
% 07.12.2020
clc 
clear all

bin_low_mid_high(1,3,2)
bin_low_mid_high(1,2,3)
bin_low_mid_high(2.5,2,3)
bin_low_mid_high(4,2,3)

function [val] = bin_low_mid_high(number, mid_thresh, high_thresh)
    if(mid_thresh > high_thresh)
        val = "User error: thresholds not strictly increasing";
    else
        if(number < mid_thresh)
            val = "low";
        elseif(number >= mid_thresh && number <= high_thresh)
            val = "mid";
        elseif(number > high_thresh)
            val = "high";
        else
            val = "Error";
        end
    end
end