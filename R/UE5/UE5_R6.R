# Author:         Lucas Mandl
# Company:        FH Joanneum
# Date:           28.10.2020
# Description:    Implementation of laboratory session 5 - R.6 Lubridate
# ============================================
rm(list = ls())       # clear workspace
cat("\014")           # clear command line
# ============================================

library(lubridate)
library(nycflights13)

flights <- nycflights13::flights

flights_corrected <- flights %>% filter(arr_time < dep_time) %>%  # filter flights with incorrect arrival time
  # add new column with correct date and time using paste (converts int to string)
  # and ymd_hm (converts a string into a date in the format yyyy-MM-dd hh:mm)
  mutate(arr_ymdhs = ymd_hm(paste(year,str_pad(month,2,"left","0"),
               str_pad(day,2,"left","0"),
               str_pad(arr_time,4,"left","0"),sep=""))+days(1))
