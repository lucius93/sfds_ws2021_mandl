# Author:         Lucas Mandl
# Company:        FH Joanneum
# Date:           24.10.2020
# Description:    Implementation of laboratory session 5 - R.3 tidy data
# ============================================
rm(list = ls())       # clear workspace
cat("\014")           # clear command line
# ============================================

library(tidyverse)

# Titanic passenger counts by Class and Sex -------------------------------
# this dataset is "not tidy", => kreuztabelle, man kann die beobachtungen und
# variablen nicht klar trennen also macht das keinen sinn das zu trennen
titantic_passengers <-
  apply(Titanic, c("Class", "Sex"), sum) %>%
  as.data.frame() %>%
  rownames_to_column() %>%
  rename(Class = rowname) %>%
  as_tibble()

titantic_passengers_ <- titantic_passengers %>% gather('Male', 'Female',
                                                       key = 'gender',
                                                       value = '#passengers')



# Diamond auction prices --------------------------------------------------
# this dataset is tidy
diamond_prices <-
  diamonds %>%
  select(carat:clarity, price)


# Age and height of different people --------------------------------------
# cannot convert to tidy data as there is two times the same age of phillip
people <- tribble(
  ~name,             ~property,    ~value,
  #-----------------|--------|------
  "Phillip Woods",   "age",       45,
  "Phillip Woods",   "height",   186,
  "Phillip Woods",   "age",       50,
  "Jessica Cordero", "age",       37,
  "Jessica Cordero", "height",   156
)

# peoplem <- people %>% spread(key = "property",
#                              value = "value")
